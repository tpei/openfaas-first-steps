# OpenFaaS-First-Steps

It's time to write our first function. But first, you must have installed the necessary tools. See here: [https://gitlab.com/openfaas-experiments/openfaas-installation](https://gitlab.com/openfaas-experiments/openfaas-installation)


## First function

So, you have setup Docker, OpenFass CLI on your laptop, and you have 2 VMs runninf (a private Docker registry and an OpenFaaS server)

We are going to create a JavaScript OpenFaaS function, but you have to know that OpenFaaS is polyglot.

```
mkdir hello-word-project
cd hello-word-project
faas-cli new hello-world --lang node
```

The OpenFaaS CLI create a full project for you:

```
.
├── hello-word-project
    ├── hello-world
    │   ├── handler.js
    │   └── package.json
    ├── hello-world.yml
    └── template
```

The `template` directory contains function templates for different languages. You can indeed use several functions with a different language in the same project. But today, we'll only use a single JavaScript function.

The `hello-world.yml` contains all the needed information to deploy the function:

```yaml
provider:
  name: faas
  gateway: http://127.0.0.1:8080
functions:
  hello-world:
    lang: node
    handler: ./hello-world
    image: hello-world:latest
```

We are going to update this file with the information of our Docker registry and OpenFaaS server:

```yaml
provider:
  name: faas
  gateway: http://openfaas.test:8080
functions:
  hello-world:
    lang: node
    handler: ./hello-world
    image: registry.test:5000/hello-world:latest
```

And as you can see, the main source code in `handler.js` is pretty simple:

```javascript
"use strict"

module.exports = (context, callback) => {
  callback(undefined, {status: "done"});
}
```

## Build, then push the function to the registry

```
faas-cli build -f hello-world.yml 
faas-cli push -f hello-world.yml 
```

> Check it: if you type `curl http://registry.test:5000/v2/_catalog` you'll get this: `{"repositories":["hello-world"]}`

## Deploy the function

In order to be able to deploy the function, we need to explain to the CLI where is the OpenFaaS server and what are the credentials:

```
export OPENFAAS_URL=http://openfaas.test:8080
echo -n d3d24b12dcd369ef6707bc810c110b74b70cdfdd4e83f8b021c3d5e19d4ca86c | faas-cli login --username=admin --password-stdin
```

See [https://gitlab.com/openfaas-experiments/openfaas-installation](https://gitlab.com/openfaas-experiments/openfaas-installation) to know how to get the credentials.

Then, you can deploy your :sparkes: function:

```
faas-cli deploy -f hello-world.yml
```

It's done :tada: and you can already test it: `curl http://openfaas.test:8080/function/hello-world` and you'll get `{"status":"done"}`

> - :wave: you can also test your function directly with the ui of the OpenFaaS server [http://openfaas.test:8080/ui/](http://openfaas.test:8080/ui/)
> - or even in using this: `echo | faas-cli invoke hello-world`

## Tips

### Environment variables

You can create environmant variables for your function(s). For example, update the `hello-world.yml` file like that:

```yaml
provider:
  name: faas
  gateway: http://openfaas.test:8080
functions:
  hello-world:
    lang: node
    environment:
      TOKEN: "ILOVEPANDA"
      MESSAGE: "👋 Hello world 🌍"
    handler: ./hello-world
    image: registry.test:5000/hello-world:latest
```

and update `handler.js`:

```javascript
"use strict"

module.exports = (context, callback) => {
  callback(undefined, {
      token: process.env['TOKEN']
    , message: process.env['MESSAGE']
  })
}
```

Then, deploy simply your updates with this command `faas-cli up -f hello-world.yml`. That's all 😀. 

Type `echo | faas-cli invoke hello-world` and you'll get 

```json
{"token":"ILOVEPANDA","message":"👋 Hello world 🌍"}
```

### Post data

You need to use `context`arguments to get "posted" data. Update `handler.js` again:

```javascript
"use strict"

module.exports = (context, callback) => {
  callback(undefined, {
      token: process.env['TOKEN']
    , message: process.env['MESSAGE']
    , data: context.length>0 ? JSON.parse(context) : {}
  })
}
```

Deploy: `faas-cli up -f hello-world.yml` and now, try this: 

```
curl -H "Content-Type: application/json" -X POST -d '{"who":"Bob Morane"}' http://openfaas.test:8080/function/hello-world
```

And you should get: 
```json
{"token":"ILOVEPANDA","message":"👋 Hello world 🌍","data":{"who":"Bob Morane"}}
```

### Path 

If you want to get your http path (the called url) you can use the `Http_Path` variable:

Update `handler.js` again:

```javascript
"use strict"

module.exports = (context, callback) => {
  callback(undefined, {
      token: process.env['TOKEN']
    , message: process.env['MESSAGE']
    , data: data: context.length>0 ? JSON.parse(context) : {}
    , path: process.env['Http_Path']
  })
}
```

Deploy: `faas-cli up -f hello-world.yml` and now, try this: 

```
curl http://openfaas.test:8080/function/hello-world/ping/pong
```

You'll get:

```json
{"token":"ILOVEPANDA","message":"👋 Hello world 🌍","data":{},"path":"/ping/pong"}
```

